<?php
$host = 'localhost';
$username = 'crmestrella';
$password = 'kyourimina';
$database = 'p8_exercise_backend';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) 
        VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$sql = "SELECT first_name, last_name, birthday FROM employee";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
    }
} else {
    echo "0 results";
}

$sql = "SELECT COUNT(*) AS num_employees FROM employee WHERE last_name LIKE 'D%'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "Number of employees whose last name starts with 'D': " . $row["num_employees"]. "<br>";
    }
} else {
    echo "0 results";
}

$sql = "SELECT first_name, last_name, address 
        FROM employee 
        WHERE employee_id = (SELECT MAX(employee_id) FROM employee)";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Address: " . $row["address"]. "<br>";
    }
} else {
    echo "0 results";
}


$sql = "UPDATE employee 
        SET address = '123 Main Street' 
        WHERE first_name = 'John'";
if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}

$sql = "DELETE FROM employee WHERE last_name LIKE 'D%'";
if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}

$conn->close();
?>