# Inventory Management System API Documentation

### Table of Contents
- [Introduction](#introduction)
- [Features](#features)
- [API Reference](#api-reference)
- [Contributing](#Contributing)
- [License](#License)

## Introduction
Welcome to the API Documentation for the Inventory Management System. This document provides comprehensive information on the endpoints, request and response formats, and authentication requirements for managing inventory data.

## Features
- **View Batch List**

    This feature allows authorized users to retrieve a comprehensive list of all batches of products available in the inventory. It facilitates easy monitoring and management of product batches.

- **Add Batch**

    This feature enables authorized users to add a new batch of products to the inventory. Users can input details such as retail product ID, batch number, expiry date, and quantity to seamlessly integrate new products into the inventory.

- **View Batch Details**

    This feature allows authorized users to view detailed information about a specific batch of products in the inventory. It provides insights into the individual attributes and quantities of products within a batch.

- **Update Batch**

    This feature permits authorized users to edit the details of an existing batch of products in the inventory. Users can modify parameters such as retail product ID, batch number, expiry date, and quantity to accurately reflect any changes or updates.

- **Delete Batch**

    This feature enables authorized users to remove a batch of products from the inventory. It ensures efficient management of inventory data by allowing users to eliminate outdated or redundant product batches.

## API Reference
The API Reference provides detailed information about the endpoints and request/response formats supported by the API.

### Endpoints

#### View Batch List
Retrieves a list of all batch details associated with the system.

**URL**: 'batch/retrieveBatch/{id}'

**Method**: 'GET'

**Headers**:

**Content-type**: 'application/json'

**Parameters**:
  -'id' (int, required): Unique identifier for the desired batch product.

**Success Response**:

  - **Status Code**: '200 OK'

  - **Response Body**:


 ```json
{
    "status": "success",
    "data": {
        "id": 4,
        "retail_product_id": 213,
        "batch_number": 178,
        "expiry_date": "2024-05-23T00:00:00",
        "quantity": 300
    },
        "method": "GET"
}
```

**Error Responses**:

  - **Status Code**: '401 Unauthorized'

  - **Response Body**:

    - Display error message for unauthorrized access.
    
        ```json
        {
            "status": "fail",
            "message": "Unauthorized Access"
        }
        ```


   - **Status Code**: '404 Not Found'

   - **Response Body**:

        - If query is unsuccessful

        ```json
        {
            "status": "fail",
            "message": "Batch not found"
        }


   - **Status Code**: '400 Bad Request'

   - **Response Body**:

        - If data is invalid.

        ```json
       {
            "status": "fail",
            "message": "Invalid request. Please provide a valid 'id' parameter."
       }


#### Add Batch List
Create a new batch entry in the inventory.

- **URL** 'batch/addBatch'
- **Method** 'POST'
- **Headers** 'batch/addBatch'
    - **Content-type** 'application/json'
- **Parameters**:
    - id: Auto-incremented ID of type int.

    - retail_product_id: ID of the retail product, of type int.

    - batch_number: Batch number of type int.

    - expire_date: Expiry date of type datetime.

    - quantity: Quantity of the product, of type int.


- **Success Response**
    
    -**Status Code**: '200 OK'
    
    -**Response Body**:

```json
{
    "status": "success",
    "data": {
        "id": 4,
        "retail_product_id": 213,
        "batch_number": 178,
        "expiry_date": "2024-08-13T00:00:00",
        "quantity": 125
    },
    "method": "POST"
}
```

- **Error Responses**
    - **Status Code**: '200 OK'
    - **Response Body**:

        - Display error if the required fileds filled and data are not properly formatted.

        ```json
        {
            "status": "fail",
            "message": "The required fileds filled and data are not properly formatted."
        }
        ```

        - Display error message for unauthorized access.

        ```json
        {
            "status": "fail",
            "message": "Unauthorized Access."
        }
        ```

        - Display error message if the data is invalid

        ```json
        {
            "status": "fail",
            "message": "Invalid Data"
        }
        ```


#### Update Batch
Allows authorized users to modify details of an existing batch entry in the inventory.

- **URL**: 'batch/updateBatch/{id}'
- **Method**: 'PUT'
- **Headers**:
    - **Content-type**: 'application/json'
- **Parameters**:
    - 'id' (int, required): Unique identifier for that certain batch product.

- **Success Response**:
    - **Status Code**: '200 OK'
    - **Response Body**:

        ```json
        {
            "status": "success",
            "data": {
                "id": 4,
                "retail_product_id": 213,
                "batch_number": 178,
                "expiry_date": "2024-05-23T00:00:00",
                "quantity": 300
            },
            "method": "PUT"
        }
        ```

- **Error Responses**:
    - **Status Code**: '200 OK'
    - **Response Body**:

        - Display error message for unauthorize accesss.
        ```json
        {
            "status": "fail",
            "message": "Unauthorized Access"
        }
        ```

        - If query is unsuccessful
        ```json
        {
            "status": "fail",
            "message": "No changes saved"
        }
        ```

        - If data is invalid
        ```json
        {
            "status": "fail",
            "message": "Invalid data input"
        }
        ```

#### Delete Batch

Permits authorized users to remove a batch entry from the inventory.

- **URL**: 'batch/{id}'
- **Method**: 'DELETE'
- **Headers**:
    - **Content-type**: 'application/json'
- **Parameters**:
    - 'id' (int, required): Unique identifier for that certain batch product.

- **Success Response**:
    - **Status Code**: '200 OK'
    - **Response Body**:

        ```json
        {
            "status": "success",
            "data": [],
            "method": "DELETE"
        }
        ```

- **Error Responses**:
    - **Status Code**: '200 OK'
    - **Response Body**:

        - Display error message for unauthorize accesss.
        ```json
        {
            "status": "fail",
            "message": "Unauthorized Access"
        }
        ```

        - If data doesn't exist
        ```json
        {
            "status": "fail",
            "message": "No data found"
        }
        ```