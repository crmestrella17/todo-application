<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $name = $_POST['name'];
  $greeting_message = "Hello, $name! Welcome to our website.";
  echo $greeting_message;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personalized Greeting</title>
</head>
<body>
    <form method="POST">
      <label for="name">Enter your name:</label>
      <input type="text" id="name" name="name">
      <button type="submit">Submit</button>
    </form>
</body>
</html>