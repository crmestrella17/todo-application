<?php
$host = 'localhost';
$username = 'crmestrella';
$password = 'kyourimina';
$database = 'p8_exercise_backend';

// Function to establish database connection
function connectToDatabase() {
    global $host, $username, $password, $database;
    $conn = new mysqli($host, $username, $password, $database);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}

// Function to create a new employee record
function createEmployee($first_name, $last_name, $middle_name, $birthday, $address) {
    $conn = connectToDatabase();
    $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) 
            VALUES ('$first_name', '$last_name', '$middle_name', '$birthday', '$address')";
    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
    $conn->close();
}

// Function to read employee records
function readEmployees() {
    $conn = connectToDatabase();
    $sql = "SELECT first_name, last_name, birthday FROM employee";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    $conn->close();
}

// Function to update an employee's address
function updateEmployeeAddress($first_name, $new_address) {
    $conn = connectToDatabase();
    $sql = "UPDATE employee 
            SET address = '$new_address' 
            WHERE first_name = '$first_name'";
    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
    $conn->close();
}

// Function to delete an employee record
function deleteEmployee($last_name) {
    $conn = connectToDatabase();
    $sql = "DELETE FROM employee WHERE last_name = '$last_name'";
    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
    $conn->close();
}

// Handle form submissions
if (isset($_SERVER["REQUEST_METHOD"]) == "POST") {
    if (isset($_POST["submit_create"])) {
        createEmployee($_POST["first_name"], $_POST["last_name"], $_POST["middle_name"], $_POST["birthday"], $_POST["address"]);
    } elseif (isset($_POST["submit_update"])) {
        updateEmployeeAddress($_POST["first_name"], $_POST["new_address"]);
    } elseif (isset($_POST["submit_delete"])) {
        deleteEmployee($_POST["last_name"]);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee Management System</title>
</head>
<body>
    <h2>Add Employee</h2>
    <form method="POST">
        <label for="first_name">First Name:</label>
        <input type="text" id="first_name" name="first_name" required><br><br>
        <label for="last_name">Last Name:</label>
        <input type="text" id="last_name" name="last_name" required><br><br>
        <label for="middle_name">Middle Name:</label>
        <input type="text" id="middle_name" name="middle_name"><br><br>
        <label for="birthday">Birthday:</label>
        <input type="date" id="birthday" name="birthday" required><br><br>
        <label for="address">Address:</label>
        <input type="text" id="address" name="address" required><br><br>
        <button type="submit" name="submit_create">Add Employee</button>
    </form>

    <hr>

    <h2>Update Employee Address</h2>
    <form method="POST">
        <label for="first_name_update">First Name:</label>
        <input type="text" id="first_name_update" name="first_name" required><br><br>
        <label for="new_address">New Address:</label>
        <input type="text" id="new_address" name="new_address" required><br><br>
        <button type="submit" name="submit_update">Update Address</button>
    </form>

    <hr>

    <h2>Delete Employee</h2>
    <form method="POST">
        <label for="last_name_delete">Last Name:</label>
        <input type="text" id="last_name_delete" name="last_name" required><br><br>
        <button type="submit" name="submit_delete">Delete Employee</button>
    </form>

    <hr>

    <h2>Employee List</h2>
    <?php readEmployees(); ?>
</body>
</html>