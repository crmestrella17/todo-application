<?php
require 'vendor/autoload.php';
use ThingEngineer\MySqliDatabase;

$host = 'localhost';
$username = 'crmestrella';
$password = 'kyourimina';
$database = 'p8_exercise_backend';

// Function to create a new employee record
function createEmployee($db, $first_name, $last_name, $middle_name, $birthday, $address) {
    $data = array(
        'first_name' => $first_name,
        'last_name' => $last_name,
        'middle_name' => $middle_name,
        'birthday' => $birthday,
        'address' => $address
    );
    $result = $db->insert('employee', $data);
    if ($result) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $db->getLastError();
    }
}

// Function to read employee records
function readEmployees($db) {
    $result = $db->get('employee', null, ['first_name', 'last_name', 'birthday']);
    if ($result) {
        foreach ($result as $row) {
            echo "First Name: " . $row["first_name"]. " - Last Name: " . $row["last_name"]. " - Birthday: " . $row["birthday"]. "<br>";
        }
    } else {
        echo "0 results";
    }
}

// Function to update an employee's address
function updateEmployeeAddress($db, $first_name, $new_address) {
    $data = array('address' => $new_address);
    $where = array('first_name' => $first_name);
    $result = $db->update('employee', $data, $where);
    if ($result) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $db->getLastError();
    }
}

// Function to delete an employee record
function deleteEmployee($db, $last_name) {
    $where = array('last_name' => $last_name);
    $result = $db->delete('employee', $where);
    if ($result) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $db->getLastError();
    }
}

// Handle form submissions
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $db = new MySqliDatabase($host, $username, $password, $database);
    if (isset($_POST["submit_create"])) {
        createEmployee($db, $_POST["first_name"], $_POST["last_name"], $_POST["middle_name"], $_POST["birthday"], $_POST["address"]);
    } elseif (isset($_POST["submit_update"])) {
        updateEmployeeAddress($db, $_POST["first_name"], $_POST["new_address"]);
    } elseif (isset($_POST["submit_delete"])) {
        deleteEmployee($db, $_POST["last_name"]);
    }
    $db->disconnect(); 
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee Management System</title>
</head>
<body>
    <h2>Add Employee</h2>
    <form method="POST">
        <label for="first_name">First Name:</label>
        <input type="text" id="first_name" name="first_name" required><br><br>
        <label for="last_name">Last Name:</label>
        <input type="text" id="last_name" name="last_name" required><br><br>
        <label for="middle_name">Middle Name:</label>
        <input type="text" id="middle_name" name="middle_name"><br><br>
        <label for="birthday">Birthday:</label>
        <input type="date" id="birthday" name="birthday" required><br><br>
        <label for="address">Address:</label>
        <input type="text" id="address" name="address" required><br><br>
        <button type="submit" name="submit_create">Add Employee</button>
    </form>

    <hr>

    <h2>Update Employee Address</h2>
    <form method="POST">
        <label for="first_name_update">First Name:</label>
        <input type="text" id="first_name_update" name="first_name" required><br><br>
        <label for="new_address">New Address:</label>
        <input type="text" id="new_address" name="new_address" required><br><br>
        <button type="submit" name="submit_update">Update Address</button>
    </form>

    <hr>

    <h2>Delete Employee</h2>
    <form method="POST">
        <label for="last_name_delete">Last Name:</label>
        <input type="text" id="last_name_delete" name="last_name" required><br><br>
        <button type="submit" name="submit_delete">Delete Employee</button>
    </form>

    <hr>

    <h2>Employee List</h2>
    <?php
    $db = new MySqliDatabase($host, $username, $password, $database);
    readEmployees($db);
    $db->disconnect(); 
    ?>
</body>
</html>
