<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once 'MysqliDb.php';

$request_method = $_SERVER['REQUEST_METHOD'];

if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $ids = null;
        $exploded_request_uri = explode("/", $request_uri);
        $last_index = count($exploded_request_uri) - 1;
        $ids = $exploded_request_uri[$last_index];
    }

    $received_data = json_decode(file_get_contents('php://input'), true);
}

$api = new API;

switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}

class API {
    private $db;

    public function __construct() {
        $this->db = new MysqliDb('localhost', 'root', '', 'employee');
    }

    private function failedResponse($method, $message) {
        echo json_encode([
            'method' => $method,
            'status' => 'failed',
            'message' => $message,
        ]);
    }

    private function successResponse($method, $data = null) {
        $response = [
            'method' => $method,
            'status' => 'success',
        ];

        if ($data) {
            $response['data'] = $data;
        }
        
        echo json_encode($response);
    }

    public function httpGet($payload) {
        if (isset($payload['id'])) {
            $this->db->where('id', $payload['id']);
        }
    
        $data = $this->db->get('tbl_to_do_list');
    
        if (isset($payload['id']) && empty($data)) {
            $this->failedResponse('GET', 'Task not found');
            return;
        }
    
        $this->successResponse('GET', $data);
    }

    public function httpPost($payload) {
        // Check if payload is a non-empty array
        if (!is_array($payload) || empty($payload)) {
            $this->failedResponse('POST', 'Payload must be a non-empty array');
            return;
        }
    
        // Check if the payload contains the primary key (id)
        if (isset($payload['id'])) {
            // Perform a check to see if a record with the same ID already exists
            $existingRecord = $this->db->where('id', $payload['id'])->getOne('tbl_to_do_list');
            if ($existingRecord) {
                // If a record with the same ID exists, return a failed response
                $this->failedResponse('POST', 'A record with the same ID already exists');
                return;
            }
        }
    
        // Perform the insertion
        $queryResult = $this->db->insert('tbl_to_do_list', $payload);
    
        // Check if insertion was successful
        if ($queryResult) {
            // If successful, return a success response
            $this->successResponse('POST', $payload);
        } else {
            // If insertion failed, return a failed response
            $this->failedResponse('POST', 'Failed to Insert Data');
        }
    }
    
    public function httpPut($id, $payload) {
        // Check if id is not null or empty
        if (empty($id)) {
            $this->failedResponse('PUT', 'ID must not be null or empty');
            return;
        }
    
        // Check if payload is not empty
        if (empty($payload)) {
            $this->failedResponse('PUT', 'Payload must not be empty');
            return;
        }
    
        // Check if the id passed in matches the id in the payload
        if ($id != $payload['id']) {
            $this->failedResponse('PUT', 'ID in the payload does not match the provided ID');
            return;
        }
    
        // Use the where() function from the MysqliDB class, specify the ID
        $this->db->where('id', $id);
    
        // Execute a query to update data in the database
        $queryResult = $this->db->update('tbl_to_do_list', $payload);
    
        // Check if the query was successful
        if ($queryResult) {
            $this->successResponse('PUT', $payload);
        } else {
            $this->failedResponse('PUT', 'Failed to Update Data');
        }
    }
    
    public function httpDelete($url) {
        $urlParts = explode('/', $url);
        $id = end($urlParts);
    
        if (empty($id)) {
            $this->failedResponse('DELETE', 'Task not found');
            return;
        }
    
        $this->db->where('id', $id);
    
        $queryResult = $this->db->delete('tbl_to_do_list');
    
        if ($queryResult) {
            $this->successResponse('DELETE', ['id' => $id]);
        } else {
            $this->failedResponse('DELETE', 'Failed to Delete Data');
        }
    }
}