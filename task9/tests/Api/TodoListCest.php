<?php

namespace Tests\Api;

use Tests\Support\ApiTester;

class FailTestTodoListCest
{
    public function testGetFailTaskNotFound(ApiTester $I)
    {
        $I->wantTo('Test failure of GET due to nonexistent id');
        // Prepare test data
        $nonExistentId = 9999;
        $payload = ['id' => $nonExistentId];

        // Make the GET request
        $I->sendGet("API.php", $payload);

        // Validate response
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'status' => 'failed',
            'message' => 'Task not found'
        ]);
    }
    public function insertFailTest(ApiTester $I) {
        $I->wantTo('Test failure of INSERT due to missing fields');

        $I->sendPOST('API.php/', [
            'task_title' => 'New Task',
            'task_name' => 'Do something',
            'status' => 'Pending',
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'method' => 'POST',
            'status' => 'failed',
            'message' => 'Payload must be a non-empty array',

        ]);
    }

    public function updateNonexistentTaskFailTest(ApiTester $I) {
        $I->wantTo('Test failure of PUT due to nonexistent task');

        $I->sendPUT(
            'API.php/23',
            ['task_title' => 'Updated Task', 'description' => 'Updated Description']
        );

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'method' => 'PUT',
            'status' => 'failed',
            'message' => 'Payload must not be empty',
        ]);
    }

    public function deleteNonExistentTaskFailTest(ApiTester $I) {
        $I->wantTo('Test failure of DELETE due to nonexistent task');

        $I->sendDELETE('API.php/');

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            'method' => 'DELETE',
            'status' => 'failed',
            'message' => 'Task not found',
        ]);
    }
}

class TodoListCest
{
    public function iShouldGetAllData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('API.php/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success',]);
    }

    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('API.php/', [
            'id' => 1234,
            'task_title' => 'New Task',
            'task_name' => 'Do something',
            'status' => "Done"
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success',]);
    }

    public function iShouldUpdateData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT('API.php/1234' , [
            'id' => 1234,
            'task_title' => 'Updated Task',
            'task_name' => 'Updated Task Name',
            'status' => 'Pending'
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success',]);
    }

    public function iShouldDeleteData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE('API.php/1234');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success',]);
    }
}

