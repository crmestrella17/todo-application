<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function iShouldGetAllData(ApiTester $I)
    {
        $I->sendGET('/');
        $I->seeResponseCodeIs(400);
        $I->seeResponseContainsJson([
            'method' => 'GET',
            'status' => 'failed',
        ]);
    }

    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/', []);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContainsJson([
            'method' => 'POST',
            'status' => 'failed',
        ]);
    }

    public function iShouldUpdateData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT('/123', []);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContainsJson([
            'method' => 'PUT',
            'status' => 'failed',
        ]);
    }

    public function iShouldDeleteData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE('/123', []);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContainsJson([
            'method' => 'DELETE',
            'status' => 'failed',
        ]);
    }
}
