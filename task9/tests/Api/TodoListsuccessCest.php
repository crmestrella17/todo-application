<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function iShouldGetAllData(ApiTester $I)
    {
        $I->sendGET('/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'method' => 'GET',
            'status' => 'success',
        ]);
    }

    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/', [
            'task_title' => 'New Task',
            'task_name' => 'Do something',
            'status' => 'Pending'
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'method' => 'POST',
            'status' => 'success',
            'message' => 'Data inserted successfully',
        ]);
    }

    public function iShouldUpdateData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT('/123', [
            'id' => 123,
            'task_title' => 'Updated Task',
            'task_name' => 'Do something else',
            'status' => 'Completed'
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'method' => 'PUT',
            'status' => 'success',
            'message' => 'Data updated successfully',
        ]);
    }

    public function iShouldDeleteData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE('/123', [
            'id' => 123
        ]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContainsJson([
            'method' => 'DELETE',
            'status' => 'success',
            'message' => 'Data deleted successfully',
        ]);
    }
}

